#!/bin/bash
SUITE_ID=0
for i in `seq 1 80`;
do
  echo "Inserting for user $i"
  for j in `seq 1 50`
  do
    echo "For user $i, inserting test $j"
    curl -X POST -F type="suite" -F state="starting" -F id="$SUITE_ID" -F repository="my-repo-name$i" "http://event:bardzotajnehaslo123@localhost:8080/event"
    echo "Spec id is now $SUITE_ID"
    for k in `seq 1 30`
    do
       echo "Inserting spec $k"
       curl -X POST -F type="spec" -F state="starting" -F suiteId="$SUITE_ID" -F name="My spec name$k" "http://event:bardzotajnehaslo123@localhost:8080/event"
       duration=`jot -r 1  1 120`
       curl -X POST -F type="spec" -F state="passed" -F suiteId="$SUITE_ID" -F duration="$duration" -F name="My spec name$k" -F message='Expected error: <*errors.errorString | 0xc8204d3c00>: { s: "Missing -app.image", } Missing -app.image not to have occurred' "http://event:bardzotajnehaslo123@localhost:8080/event"
    done
    curl -X POST -F type="suite" -F state="passed" -F id="$SUITE_ID" -F duration="0.003655" -F failed="16" -F passed="0" -F pending="1" -F total="17" "http://event:bardzotajnehaslo123@localhost:8080/event"
    let SUITE_ID=SUITE_ID+1
  done
done    
