package pl.hackwaw.stats.context.repository;

import lombok.extern.slf4j.Slf4j;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

@Component
@Slf4j
public class Influx {

    private InfluxDB influxDB;

    @PostConstruct
    public void init() {
        influxDB = InfluxDBFactory.connect("http://localhost:8086", "root", "root");
    }

    public void save(String repository, Integer points) {
        Point point = Point.measurement("scores")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .field("value", points)
                .tag("repository", repository)
                .build();
        influxDB.write("hackwaw", "default", point);
        log.info("Should store repository '{}' with points '{}'", repository, points);
    }

}
