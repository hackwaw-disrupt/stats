package pl.hackwaw.stats.context.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import pl.hackwaw.stats.context.domain.Suite;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.stream.Stream;

import static org.springframework.data.jpa.repository.EntityGraph.EntityGraphType.LOAD;

public interface SuiteRepository extends JpaRepository<Suite, Long> {

//    @EntityGraph(value = "Suite.detail", type = LOAD)
    Optional<Suite> findBySuiteId(Long suiteId);

    Stream<Suite> findByRepositoryOrderByDateTimeDesc(String repositoryId);
}
