package pl.hackwaw.stats.context.domain.value;

public enum StateType {
    starting, passed, failed
}
