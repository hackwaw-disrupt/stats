package pl.hackwaw.stats.context.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.hackwaw.stats.context.domain.value.CIRequest;
import pl.hackwaw.stats.context.domain.value.StateType;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@NamedEntityGraph(name = "Suite.detail",
        attributeNodes = @NamedAttributeNode("specs"))
public class Suite {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, unique = true)
    private Long suiteId;

    @Column(nullable = false)
    private String repository;

    @Column(nullable = false)
    private StateType state;

    @Column(nullable = false)
    private LocalDateTime dateTime;

    private Double duration;

    private Integer failed;

    private Integer passed;

    private Integer pending;

    private Integer total;

    private Integer points;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
    @JoinColumn(name = "build_id")
    @OrderBy("testName ASC")
    private List<Spec> specs = new ArrayList<>();

    public Suite(CIRequest request) {
        this.repository = request.getRepository();
        this.suiteId = request.getId();
        this.state = request.getState();
        this.dateTime = LocalDateTime.now();
    }

    public Integer getPoints() {
        if (points == null) {
            return sumPoints();
        } else {
            return points;
        }
    }

    public Integer sumPoints() {
        return specs.stream().mapToInt(Spec::getPoints).sum();
    }

    public void savePoints() {
        points = sumPoints();
    }
}
