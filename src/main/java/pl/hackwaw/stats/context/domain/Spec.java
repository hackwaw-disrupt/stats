package pl.hackwaw.stats.context.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import pl.hackwaw.stats.context.domain.value.StateType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class Spec {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, length = 512)
    private String testName;

    @Column(nullable = false)
    private StateType stateType;

    @Column(nullable = false)
    private LocalDateTime dateTime;

    private Double duration;

    @Lob
    @Type(type="org.hibernate.type.StringClobType")
    private String message;

    public Spec(String testName, Double duration, StateType stateType, LocalDateTime date) {
        this.testName = testName;
        this.duration = duration;
        this.stateType = stateType;
        this.dateTime = date;
    }

    public Integer getPoints() {
        if (stateType != StateType.passed || duration == null) {
            return 0;
        }
        return passingPoints() + timePoints(duration) + difficultyPoints(testName);
    }

    private Integer timePoints(Double duration) {
        return Math.max(0, (60 - duration.intValue())/2);
    }

    private Integer passingPoints() {
        return 60;
    }

    private Integer difficultyPoints(String testName) {
        return 0;
    }
}
