package pl.hackwaw.stats.context.domain.value;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CIRequest {
    //Spec and suite
    private RequestType type;
    private StateType state;
    private Double duration;

    //Suite only
    private String repository;
    private Long id;
    private Integer failed;
    private Integer passed;
    private Integer pending;
    private Integer total;

    //Spec only
    private Long suiteId;
    private String name;
    private String message;
}