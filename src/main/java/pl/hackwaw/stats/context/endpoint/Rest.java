package pl.hackwaw.stats.context.endpoint;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.hackwaw.stats.context.domain.Suite;
import pl.hackwaw.stats.context.repository.SuiteRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
public class Rest {

    @Autowired
    private SuiteRepository suiteRepository;

    @CrossOrigin
    @RequestMapping(value = "/api/suite", method = RequestMethod.GET)
    private List<SuiteSummary> suite() {

        return suiteRepository
                .findAll()
                .stream()
                .sorted((s1, s2) -> Integer.compare(s2.getPoints(), s1.getPoints()))
                .map(suite -> new SuiteSummary(suite.getRepository(), suite.getPoints(), suite.getDateTime()))
                .collect(Collectors.toList());
    }

    @CrossOrigin
    @RequestMapping(value = "/api/recalculatePoints", method = RequestMethod.GET)
    private void recalculatePoints() {
        List<Suite> suites = suiteRepository
                .findAll()
                .stream()
                .map(e -> {
                    Integer points = e.sumPoints();
                    e.setPoints(points);
                    return e;
                }).collect(Collectors.toList());
        suiteRepository.save(suites);
    }
}

@Data
@AllArgsConstructor
@NoArgsConstructor
class SuiteSummary {
    private String reposiotory;
    private Integer points;
    private LocalDateTime timestamp;
}
