package pl.hackwaw.stats.context.endpoint;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.hackwaw.stats.context.domain.Spec;
import pl.hackwaw.stats.context.domain.Suite;
import pl.hackwaw.stats.context.domain.value.CIRequest;
import pl.hackwaw.stats.context.repository.Influx;
import pl.hackwaw.stats.context.repository.SuiteRepository;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

@Slf4j
@RestController
public class CIRequestHandler {

    @Autowired
    private SuiteRepository suiteRepository;

    @Autowired
    private Influx influx;

    @CrossOrigin
    @RequestMapping(value = "/event", method = RequestMethod.POST)
    private void event(@ModelAttribute CIRequest request) {
        switch (request.getType()) {
            case spec: {
                switch (request.getState()) {
                    case starting:
                        handleStartingSpec(request);
                        break;
                    case passed:
                    case failed:
                        handlePassedFailedSpec(request);
                        break;
                    default:
                        throw new RuntimeException("Unknown state type in spec type");
                }
                break;
            }
            case suite: {
                switch (request.getState()) {
                    case starting:
                        handleStartingSuite(request);
                        break;
                    case passed:
                    case failed:
                        handlePassedFailedSuite(request);
                        break;
                    default:
                        throw new RuntimeException("Unknown state type in suite type");
                }
                break;
            }
            default:
                throw new RuntimeException("Unknown spec type");
        }

    }

    private void handlePassedFailedSuite(CIRequest request) {
        Suite suite = suiteRepository.findBySuiteId(request.getId()).orElseThrow(() -> new RuntimeException("Suite with id '" + request.getId() + "' not found."));
        suite.setDuration(request.getDuration());
        suite.setFailed(request.getFailed());
        suite.setPending(request.getPending());
        suite.setPassed(request.getPassed());
        suite.setTotal(request.getTotal());
        suite.setState(request.getState());
        suite.setDateTime(LocalDateTime.now());
        suite.savePoints();
        suiteRepository.save(suite);
        influx.save(suite.getRepository(), suite.getPoints());
    }

    private void handlePassedFailedSpec(CIRequest request) {
        Suite suite = suiteRepository.findBySuiteId(request.getSuiteId()).orElseThrow(() -> new RuntimeException(String.format("Suite with id '%d' not found.", request.getSuiteId())));
        Spec dbSpec = suite.getSpecs().stream().filter(spec -> request.getName().equals(spec.getTestName())).collect(Collectors.reducing((a, b) -> null)).orElseThrow(() -> new RuntimeException(String.format("Suite with id '%d', does not contains spec with id '%s'", request.getSuiteId(), request.getName())));
        dbSpec.setDateTime(LocalDateTime.now());
        dbSpec.setDuration(request.getDuration());
        dbSpec.setMessage(request.getMessage());
        dbSpec.setStateType(request.getState());
        suite.setDateTime(LocalDateTime.now());
        suiteRepository.save(suite);
    }

    private void handleStartingSpec(CIRequest request) {
        Suite suite = suiteRepository.findBySuiteId(request.getSuiteId()).orElseThrow(() -> new RuntimeException("Suite with id '" + request.getSuiteId() + "' not found."));
        suite.getSpecs().add(new Spec(request.getName(), request.getDuration(), request.getState(), LocalDateTime.now()));
        suite.setDateTime(LocalDateTime.now());
        suiteRepository.save(suite);
    }

    private void handleStartingSuite(CIRequest request) {
        Suite b = new Suite(request);
        suiteRepository.save(b);
    }

}