package pl.hackwaw.stats.context.endpoint;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.hackwaw.stats.context.domain.Suite;
import pl.hackwaw.stats.context.repository.SuiteRepository;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Controller
public class ViewEndpoint {

    @Autowired
    private SuiteRepository suiteRepository;

    @CrossOrigin
    @RequestMapping(value = "/", method = RequestMethod.GET)
    private String suite(Model model) {

        List<Suite> suiteList = suiteRepository
                .findAll()
                .stream()
                .collect(Collectors.groupingBy(Suite::getRepository))
                .entrySet()
                .stream()
                .map(e -> e.getValue().stream().max((s1, s2) -> Integer.compare(s1.getPoints(), s2.getPoints())).get())
                .sorted((s1, s2) -> Integer.compare(s2.getPoints(), s1.getPoints()))
                .collect(Collectors.toList());

        model.addAttribute("title", "Najlepsze buildy w każdej kategorii");
        model.addAttribute("suits", suiteList);
        return "suits";
    }

    @CrossOrigin
    @RequestMapping(value = "/top100", method = RequestMethod.GET)
    private String best(Model model) {
        List<Suite> size = suiteRepository
                .findAll()
                .stream()
                .sorted((s1, s2) -> Integer.compare(s2.getPoints(), s1.getPoints()))
                .limit(100)
                .collect(Collectors.toList());
        model.addAttribute("title", "100 najlepszych buildów");
        model.addAttribute("suits", size);
        return "suits";
    }


    @CrossOrigin
    @RequestMapping(value = "/suite", method = RequestMethod.GET)
    private String suiteByRepo(@RequestParam("repositoryId") String repositoryId, Model model) {
        List<Suite> size = suiteRepository
                .findByRepositoryOrderByDateTimeDesc(repositoryId)
                .collect(Collectors.toList());
        model.addAttribute("suits", size);
        model.addAttribute("title", String.format("Buildy dla repozytorium '%s'", repositoryId));
        return "suits";
    }

    @CrossOrigin
    @RequestMapping(value = "/specs", method = RequestMethod.GET)
    private String specs(@RequestParam("id") Long id, Model model) {
        Suite suite = suiteRepository.findBySuiteId(id).orElseThrow(() -> new RuntimeException("Suite with id '" + id + "' not found."));
        model.addAttribute("specs", suite.getSpecs());
        return "specs";
    }

}