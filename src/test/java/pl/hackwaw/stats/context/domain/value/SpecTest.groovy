package pl.hackwaw.stats.context.domain.value

import pl.hackwaw.stats.context.domain.Spec
import spock.lang.Specification
import spock.lang.Unroll

import static pl.hackwaw.stats.context.domain.value.StateType.failed
import static pl.hackwaw.stats.context.domain.value.StateType.passed
import static pl.hackwaw.stats.context.domain.value.StateType.starting

class SpecTest extends Specification {

    def "given failing or starting points no points should be added"() {
        given:
            Spec s = Spec.builder().stateType(stateType).build();
        when:
            Integer points = s.getPoints()
        then:
            points == 0
        where:
            stateType << [failed, starting]
    }

    def "passing, but without time should have zero points"() {
        given:
            Spec s = Spec.builder().stateType(passed).build();
        when:
            Integer points = s.getPoints()
        then:
            points == 0
    }

    @Unroll("With '#duration' duration, and testName '#name', score should be '#result'")
    def "passing, should be with points"() {
        given:
            Spec s = Spec.builder().stateType(passed).duration(duration).testName(name).build();
        when:
            Integer points = s.getPoints()
        then:
            points == result
        where:
            duration | name  | result
            0.0      | "Nam" | 90
            30.0     | "Nam" | 75
            90.0     | "Nam" | 60
            150.0    | "Nam" | 60

    }
}
