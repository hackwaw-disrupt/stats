package pl.hackwaw.stats.context.endpoint;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.hackwaw.stats.context.domain.Spec;
import pl.hackwaw.stats.context.domain.Suite;

import java.util.Optional;
import java.util.stream.Stream;

public interface SpecRepository extends JpaRepository<Spec, Long> {}
