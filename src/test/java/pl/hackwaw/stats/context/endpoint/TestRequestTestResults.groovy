package pl.hackwaw.stats.context.endpoint

import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.boot.test.TestRestTemplate
import org.springframework.boot.test.WebIntegrationTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.client.RestTemplate
import pl.hackwaw.stats.Application
import pl.hackwaw.stats.context.domain.Spec
import pl.hackwaw.stats.context.domain.Suite
import pl.hackwaw.stats.context.repository.SuiteRepository
import spock.lang.Specification

import javax.transaction.Transactional

import static pl.hackwaw.stats.context.domain.value.StateType.failed
import static pl.hackwaw.stats.context.domain.value.StateType.starting

@Slf4j
@SpringApplicationConfiguration(Application)
@WebIntegrationTest('server.port:0')
@ActiveProfiles(value = "test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class TestRequestTestResults extends Specification {

    public static final String repositoryName = "some-team/some-group"
    public static final String testName = "My spec name"
    public static  final String messageName = "Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred"
    public static  final String longMessage = "Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred; Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred; Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred; Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred;Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred;Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred; Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred;Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred; Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred; Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred;Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred;Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred; Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred; Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred; Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred; Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred;Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred;Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred; Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred;Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred; Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred; Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred;Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred;Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred; Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred; Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred; Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred; Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred;Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred;Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred; Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred;Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred; Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred; Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred;Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred;Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred; Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred; Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred; Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred; Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred;Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred;Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred; Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred;Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred; Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred; Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred;Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred;Expected error: <*errors.errorString | 0xc8204d3c00>: { s: \"Missing -app.image\", } Missing -app.image not to have occurred"
    public static final String duration = "0.003655"

    @Value("\${local.server.port}")
    int serverPort

    RestTemplate template = new TestRestTemplate();

    @Autowired
    SuiteRepository suiteRepository

    def setup() {
        log.info('App server on port {}', serverPort)
    }

    def "should save starting build request"() {
        when:
            doRequest(startingSuiteRequest())

        then:
            List<Suite> collect = suiteRepository.findAll()
            collect.size() == 1
            Suite suite = collect.first();
            suite.repository == repositoryName
            suite.suiteId == 1L
            suite.state == starting
    }

    @Transactional
    def "should save for starting spec"() {
        given:
            doRequest(startingSuiteRequest())
        when:
            doRequest(startingSpecRequest())

        then:
            List<Suite> collect = suiteRepository.findAll()
            collect.size() == 1
            Suite suite = collect.first();
            suite.repository == repositoryName
            suite.suiteId == 1L
            suite.state == starting
            suite.specs.size() == 1
        then:
            Spec spec = suite.specs.first()
            spec.testName == testName
            spec.stateType == starting
    }

    @Transactional
    def "should save for finishing spec"() {
        given:
            doRequest(startingSuiteRequest())
            doRequest(startingSpecRequest())
        when:
            doRequest(finishingSpecRequest())

        then:
            List<Suite> collect = suiteRepository.findAll()
            collect.size() == 1
            Suite suite = collect.first();
            suite.repository == repositoryName
            suite.suiteId == 1L
            suite.state == starting
            suite.specs.size() == 1
        then:
            Spec spec = suite.specs.first()
            spec.testName == testName
            spec.stateType == failed
            spec.message == messageName
            spec.duration == Double.valueOf(duration)
    }

    @Transactional
    def "should save for finishing spec with long messages"() {
        given:
            doRequest(startingSuiteRequest())
            doRequest(startingSpecRequest())
        when:
            doRequest(finishingSpecRequest(longMessage))

        then:
            List<Suite> collect = suiteRepository.findAll()
            collect.size() == 1
            Suite suite = collect.first();
            suite.repository == repositoryName
            suite.suiteId == 1L
            suite.state == starting
            suite.specs.size() == 1
        then:
            Spec spec = suite.specs.first()
            spec.testName == testName
            spec.stateType == failed
            spec.message == longMessage
            spec.duration == Double.valueOf(duration)
    }



    def "should save for finishing suite"() {
        given:
            doRequest(startingSuiteRequest())
            doRequest(startingSpecRequest())
            doRequest(finishingSpecRequest())
        when:
            doRequest(finishingSuiteRequest())

        then:
            List<Suite> collect = suiteRepository.findAll()
            collect.size() == 1
            Suite suite = collect.first();
            suite.repository == repositoryName
            suite.suiteId == 1L
            suite.state == failed
            suite.duration == Double.valueOf(duration)
            suite.failed == 16
            suite.passed == 0
            suite.pending == 1
            suite.total == 17
    }

    private static MultiValueMap finishingSuiteRequest() {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("type", "suite");
        map.add("state", "failed");
        map.add("id", "1");
        map.add("duration", duration);
        map.add("failed", "16");
        map.add("passed", "0");
        map.add("pending", "1");
        map.add("total", "17");
        return map;
    }

    private static MultiValueMap finishingSpecRequest(String message = messageName) {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("type", "spec");
        map.add("state", "failed");
        map.add("suiteId", "1");
        map.add("message", message);
        map.add("duration", duration);
        map.add("name", testName);
        return map;
    }

    private static MultiValueMap startingSuiteRequest() {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("type", "suite");
        map.add("state", "starting");
        map.add("id", "1");
        map.add("repository", repositoryName);
        return map;
    }

    private static MultiValueMap startingSpecRequest() {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("type", "spec");
        map.add("state", "starting");
        map.add("suiteId", "1");
        map.add("name", testName)
        return map;
    }

    private String doRequest(MultiValueMap multiValueMap) {
        template.postForObject("http://localhost:" + serverPort + "/event", multiValueMap, String)
    }

}

