## Docker compose
`docker-compose up` - podniesie graphane i bazę influxdb

## Dostępy
* localhost:8086 - panel influxa
* localhost:3000 - panel grafany

## Jak wpychać dane:
Przykładowe dane:
`curl -i -XPOST 'http://localhost:8086/write?db=hackwaw' --data-binary 'scores,team=kozaki,language=java value=33 1434055544000000000'`
`curl -i -XPOST 'http://localhost:8086/write?db=hackwaw' --data-binary 'scores,team=kozaki,language=java value=25 1434055511000000000'`
`curl -i -XPOST 'http://localhost:8086/write?db=hackwaw' --data-binary 'scores,team=kozaki,language=java value=21 1434055563000000000'`
`curl -i -XPOST 'http://localhost:8086/write?db=hackwaw' --data-binary 'scores,team=kozaki,language=java value=87 1434055599000000000'`
`curl -i -XPOST 'http://localhost:8086/write?db=hackwaw' --data-binary 'scores,team=lewaki,language=python value=1 1434055539000000000'`
`curl -i -XPOST 'http://localhost:8086/write?db=hackwaw' --data-binary 'scores,team=lewaki,language=python value=12 1434055556000000000'`
`curl -i -XPOST 'http://localhost:8086/write?db=hackwaw' --data-binary 'scores,team=lewaki,language=python value=23 1434055587000000000'`

## Pre steps
* Wejdź do grafany i skonfiguruj datasource do influxa tak jak na zdjęciu
![Influx - config](graphana-ds.png)
* Dodaj query -- `SELECT last("value") FROM "scores" WHERE $timeFilter GROUP BY time($interval), "team"` lub zaimportuj [dashboard](graphana-dashobard.json)

# Przykład
Poniżej przykładowy dashboard:
![Na zdjęciu](example.png)

## Postaw ręcznie:
`set -x JDBC_DATABASE_URL jdbc:postgresql://localhost/stats`
`set -x EVENT_PASSWORD tajnehaslo`
Na heorku: `heroku config:set EVENT_PASSWORD=tajnehaslo`

### Happy path
## Start suita
`curl -v -X POST -F type="suite" -F state="starting" -F id="1" -F repository="some-team/some-group" "http://event:tajnehaslo@localhost:8080/event"`
## Start testu
`curl -v -X POST -F type="spec" -F state="starting" -F suiteId="1" -F name="My spec name" "http://event:tajnehaslo@localhost:8080/event"`
## Koniec testu
`curl -v -X POST -F type="spec" -F state="failed" -F suiteId="1" -F duration="0.003655" -F name="My spec name" -F message='Expected error: <*errors.errorString | 0xc8204d3c00>: { s: "Missing -app.image", } Missing -app.image not to have occurred' "http://event:tajnehaslo@localhost:8080/event"`
## Koniec suita
`curl -v -X POST -F type="suite" -F state="failed" -F id="1" -F duration="0.003655" -F failed="16" -F passed="0" -F pending="1" -F total="17" "http://event:tajnehaslo@localhost:8080/event"`

//TODO Postaw gdzieś influxa
//TODO Strzał do Influxa
//TODO Rób regularne backupy bazy